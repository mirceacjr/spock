package com.assignment.spock.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Set;

@Data
public class UserInfoResponse {
    @JsonProperty
    private String firstName;

    @JsonProperty
    private String lastName;

    @JsonProperty
    private double balance;

    @JsonProperty
    private Set<Transaction> transactions;

    public UserInfoResponse(String firstName, String lastName, double balance, Set<Transaction> transactions) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.balance = balance;
        this.transactions = transactions;
    }

    @Data
    public static class Transaction {
        @JsonProperty
        private String timestamp;

        @JsonProperty
        private Long accountId;

        @JsonProperty
        private double amount;

        public Transaction(String timestamp, Long accountId, double amount) {
            this.timestamp = timestamp;
            this.accountId = accountId;
            this.amount = amount;
        }
    }
}
