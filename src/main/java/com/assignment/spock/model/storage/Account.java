package com.assignment.spock.model.storage;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "accounts")
@Data
@EqualsAndHashCode(exclude = "user")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private double balance;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Account(User user) {
        this.user = user;
    }

    public Account() {
    }
}
