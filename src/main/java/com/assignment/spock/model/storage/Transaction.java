package com.assignment.spock.model.storage;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "transactions")
@Data
@EqualsAndHashCode(exclude = {"user",  "account"})
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private double amount;

    @Column(nullable = false)
    private LocalDateTime timestamp;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    public Transaction(User user, Account account, double amount, LocalDateTime timestamp) {
        this.user = user;
        this.account = account;
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public Transaction() {
    }
}
