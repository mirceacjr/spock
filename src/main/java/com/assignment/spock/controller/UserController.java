package com.assignment.spock.controller;

import com.assignment.spock.model.response.UserInfoResponse;
import com.assignment.spock.model.storage.Account;
import com.assignment.spock.model.storage.User;
import com.assignment.spock.service.TransactionService;
import com.assignment.spock.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class UserController {
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    private UserService userService;
    private TransactionService transactionService;

    @RequestMapping(method = RequestMethod.POST, value = "/users/{userId}/accounts")
    @ResponseStatus(value = HttpStatus.CREATED)
    @Transactional
    public Long createAccount(@PathVariable long userId, @RequestParam(required = false, defaultValue = "0.0") double initialCredit) {
        LOG.info("Got request to create account for userId: {} with initial credit: {}", userId, initialCredit);

        User user = userService.retrieveUser(userId);
        Account account = userService.createAccount(user);
        LOG.info("Account with id: {} created for userId: {}", account.getId(), userId);

        if (initialCredit != 0.0) {
            LOG.info("Transferring initial credit of amount: {} for user with id: {} into account with id {}", initialCredit, user.getId(), account.getId());
            transactionService.transfer(user, account, initialCredit);
        }

        return account.getId();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users/{userId}")
    @ResponseStatus(value = HttpStatus.OK)
    public UserInfoResponse getUserInformation(@PathVariable Long userId) {
        LOG.info("Got request to retrieve data for user with userId: {}", userId);

        User user = userService.retrieveUser(userId);
        Set<UserInfoResponse.Transaction> transactionResponses = transactionService.retrieveTransactions(user).stream()
                .map(transaction -> new UserInfoResponse.Transaction(transaction.getTimestamp().toString(),
                        transaction.getAccount().getId(), transaction.getAmount()))
                .sorted(Comparator.comparing(UserInfoResponse.Transaction::getTimestamp))
                .collect(Collectors.toCollection(LinkedHashSet::new));

        return new UserInfoResponse(user.getFirstName(), user.getLastName(), user.getAccounts().stream().mapToDouble(Account::getBalance).sum(),
                transactionResponses);
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }
}
