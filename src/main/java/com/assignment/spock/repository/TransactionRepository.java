package com.assignment.spock.repository;

import com.assignment.spock.model.storage.Transaction;
import org.springframework.data.repository.Repository;

import java.util.stream.Stream;

public interface TransactionRepository extends Repository<Transaction, Long> {
    Transaction save(Transaction transaction);
    Stream<Transaction> findAllByUserId(long userId);
}
