package com.assignment.spock.repository;

import com.assignment.spock.model.storage.User;
import org.springframework.data.repository.Repository;

import java.util.Optional;

public interface UserRepository extends Repository<User, Long> {
    Optional<User> findById(long id);
    User save(User account);
}
