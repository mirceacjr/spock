package com.assignment.spock.repository;

import com.assignment.spock.model.storage.Account;
import org.springframework.data.repository.Repository;

public interface AccountRepository extends Repository<Account, Long> {
    Account save(Account account);
}
