package com.assignment.spock.service;

import com.assignment.spock.model.storage.Account;
import com.assignment.spock.model.storage.Transaction;
import com.assignment.spock.model.storage.User;
import com.assignment.spock.repository.AccountRepository;
import com.assignment.spock.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TransactionService {
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    private TransactionRepository transactionRepository;
    private AccountRepository accountRepository;

    @Transactional
    public Transaction transfer(User user, Account account, double amount) {
        LOG.info("Performing transfer for user with id: {}, account id: {} and amount: {}", user.getId(), account.getId(), amount);
        return transactionRepository.save(new Transaction(user, changeAccountBalance(user, account, amount), amount, LocalDateTime.now()));
    }

    @Transactional
    public Set<Transaction> retrieveTransactions(User user) {
        LOG.info("Retrieving transactions for user with id: {}", user.getId());
        return transactionRepository.findAllByUserId(user.getId()).collect(Collectors.toSet());
    }

    private Account changeAccountBalance(User user, Account account, double amount) {
        LOG.info("Changing balance for user with id: {} in account with id: {} with amount: {}", user.getId(), account.getId(), amount);
        account.setBalance(account.getBalance() + amount);
        return accountRepository.save(account);
    }

    @Autowired
    public void setTransactionRepository(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }
}
