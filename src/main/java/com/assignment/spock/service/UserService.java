package com.assignment.spock.service;

import com.assignment.spock.exception.UserNotFoundException;
import com.assignment.spock.model.storage.Account;
import com.assignment.spock.model.storage.User;
import com.assignment.spock.repository.AccountRepository;
import com.assignment.spock.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    private UserRepository userRepository;
    private AccountRepository accountRepository;

    public User retrieveUser(long userId) {
        LOG.info("Finding user with id: {}", userId);
        return userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException("User with id " + userId + " not found..."));
    }

    public Account createAccount(User user) {
        LOG.info("Creating new account for user with id: {}", user.getId());
        return accountRepository.save(new Account(user));
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }
}
