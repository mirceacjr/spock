DROP TABLE users;
DROP TABLE accounts;
DROP TABLE transactions;

CREATE TABLE users (
  id INT AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL
);

CREATE TABLE accounts (
  id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT NOT NULL,
  balance NUMERIC(10, 2),
  CONSTRAINT fk_account_user FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE transactions (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    account_id INT NOT NULL,
    amount NUMERIC(10, 2) NOT NULL,
    timestamp TIMESTAMP NOT NULL,
    CONSTRAINT fk_transaction_user FOREIGN KEY (user_id) REFERENCES users (id),
    CONSTRAINT fk_transaction_account FOREIGN KEY (account_id) REFERENCES accounts (id)
);

INSERT INTO users (first_name, last_name) VALUES
  ('Tom', 'Cat'),
  ('Jerry', 'Mouse');