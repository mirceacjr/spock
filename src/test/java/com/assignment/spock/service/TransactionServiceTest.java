package com.assignment.spock.service;

import com.assignment.spock.model.storage.Account;
import com.assignment.spock.model.storage.Transaction;
import com.assignment.spock.model.storage.User;
import com.assignment.spock.repository.AccountRepository;
import com.assignment.spock.repository.TransactionRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TransactionServiceTest {
    private TransactionService transactionService;

    @Mock
    private TransactionRepository transactionRepository;
    @Mock
    private AccountRepository accountRepository;

    @Before
    public void setup() {
        transactionService = new TransactionService();
        initMocks(this);

        transactionService.setAccountRepository(accountRepository);
        transactionService.setTransactionRepository(transactionRepository);
    }

    @Test
    public void testTransfer_OK() {
        User user = new User();
        Account account = new Account();
        double amount = 5.0;

        Account changedAccount = new Account();
        changedAccount.setBalance(amount);
        when(accountRepository.save(account)).thenReturn(changedAccount);

        transactionService.transfer(user, account, amount);

        verify(accountRepository).save(eq(account));
        assertEquals(amount, changedAccount.getBalance(), 0.0);

        ArgumentCaptor<Transaction> transactionCaptor = ArgumentCaptor.forClass(Transaction.class);
        verify(transactionRepository).save(transactionCaptor.capture());

        assertEquals(0, transactionCaptor.getValue().getId());
        assertEquals(amount, transactionCaptor.getValue().getAmount(), 0.0);
        assertEquals(user, transactionCaptor.getValue().getUser());
        assertEquals(account, transactionCaptor.getValue().getAccount());
    }

    @Test
    public void retrieveTransactions_OK() {
        User user = new User();

        Transaction t = new Transaction();
        when(transactionRepository.findAllByUserId(0)).thenReturn(Stream.of(t));

        Set<Transaction> result = transactionService.retrieveTransactions(user);

        verify(transactionRepository).findAllByUserId(user.getId());
        assertEquals(Collections.singleton(t), result);
    }
}
