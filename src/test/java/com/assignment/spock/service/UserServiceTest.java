package com.assignment.spock.service;

import com.assignment.spock.exception.UserNotFoundException;
import com.assignment.spock.model.storage.Account;
import com.assignment.spock.model.storage.User;
import com.assignment.spock.repository.AccountRepository;
import com.assignment.spock.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class UserServiceTest {
    private UserService userService;

    @Mock
    private UserRepository userRepository;
    @Mock
    private AccountRepository accountRepository;

    @Before
    public void setup() {
        userService = new UserService();
        initMocks(this);

        userService.setUserRepository(userRepository);
        userService.setAccountRepository(accountRepository);
    }

    @Test
    public void retrieveUser_OK() {
        User user = new User();
        when(userRepository.findById(1)).thenReturn(Optional.of(user));

        User result = userService.retrieveUser(1);

        assertEquals(result, user);
        verify(userRepository).findById(eq(1L));
    }

    @Test(expected = UserNotFoundException.class)
    public void retrieveUser_NotFound() {
        when(userRepository.findById(1)).thenReturn(Optional.ofNullable(null));

        userService.retrieveUser(1);
    }

    @Test
    public void createAccount_OK() {
        User user = new User();

        Account account = new Account();
        account.setId(0);
        account.setUser(user);
        when(accountRepository.save(account)).thenReturn(account);

        Account result = userService.createAccount(user);

        assertEquals(account, result);
        verify(accountRepository).save(eq(account));
    }

    @Test
    public void createAccount_InitialBalance_OK() {
        User user = new User();

        Account account = new Account();
        account.setId(0);
        account.setUser(user);
        when(accountRepository.save(account)).thenReturn(account);

        Account result = userService.createAccount(user);

        assertEquals(account, result);

        verify(accountRepository).save(eq(account));
    }
}
