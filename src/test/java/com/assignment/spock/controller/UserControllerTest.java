package com.assignment.spock.controller;

import com.assignment.spock.exception.UserNotFoundException;
import com.assignment.spock.model.storage.Account;
import com.assignment.spock.model.storage.Transaction;
import com.assignment.spock.model.storage.User;
import com.assignment.spock.service.TransactionService;
import com.assignment.spock.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {
	@MockBean
	private UserService userService;
	@MockBean
	private TransactionService transactionService;

	@Autowired
	private MockMvc mvc;

	@Before
	public void setup() {
		initMocks(this);
	}

	@Test
	public void retrieveUserInformation_OK() throws Exception {
		User user = new User();
		Account account = new Account();
		account.setBalance(10.0);
		account.setId(1);
		user.setAccounts(Collections.singleton(account));
		user.setFirstName("Tom");
		user.setLastName("Cat");
		when(userService.retrieveUser(1)).thenReturn(user);

		Set<Transaction> transactions = new LinkedHashSet<>();
		Transaction transaction = new Transaction();
		transaction.setAmount(10.0);
		LocalDateTime now = LocalDateTime.now();
		transaction.setTimestamp(now);
		transaction.setAccount(account);
		transactions.add(transaction);
		when(transactionService.retrieveTransactions(user)).thenReturn(transactions);

		MvcResult result = mvc.perform(buildGetInformationRequest(1)).andReturn();

		assertEquals("{\"firstName\":\"Tom\",\"lastName\":\"Cat\",\"balance\":10.0,\"transactions\":[{\"timestamp\":\""
				+ now.toString() + "\",\"accountId\":1,\"amount\":10.0}]}", result.getResponse().getContentAsString());
		verify(userService).retrieveUser(eq(1L));
		verify(transactionService).retrieveTransactions(eq(user));
	}

	@Test
	public void retrieveUserInformation_invalidUserId() throws Exception {
		when(userService.retrieveUser(0)).thenThrow(new UserNotFoundException(""));

		MvcResult result = mvc.perform(buildGetInformationRequest(0)).andReturn();

		assertEquals(NOT_FOUND.value(), result.getResponse().getStatus());
		verifyZeroInteractions(transactionService);
	}

	@Test
	public void createAccount_OK() throws Exception {
		Account newAccount = new Account();
		newAccount.setId(2);
		newAccount.setBalance(0.0);

		User user = new User();
		when(userService.retrieveUser(1)).thenReturn(user);
		when(userService.createAccount(user)).thenReturn(newAccount);

		MvcResult result = mvc.perform(buildCreateAccountRequest(1, 0.0)).andReturn();

		assertEquals(2, Long.parseLong(result.getResponse().getContentAsString()));
		verify(userService).retrieveUser(eq(1L));
		verify(userService).createAccount(eq(user));
	}

	@Test
	public void createAccount_InitialCredit_OK() throws Exception {
		Account newAccount = new Account();
		newAccount.setId(2);
		newAccount.setBalance(0.0);

		User user = new User();
		when(userService.retrieveUser(1)).thenReturn(user);
		when(userService.createAccount(user)).thenReturn(newAccount);

		MvcResult result = mvc.perform(buildCreateAccountRequest(1, 10.0)).andReturn();

		assertEquals(2, Long.parseLong(result.getResponse().getContentAsString()));
		verify(userService).retrieveUser(eq(1L));
		verify(userService).createAccount(eq(user));
		verify(transactionService).transfer(eq(user), eq(newAccount), eq(10.0));
	}

	@Test
	public void createAccount_invalidUserId() throws Exception {
		when(userService.retrieveUser(1)).thenThrow(new UserNotFoundException(""));
		MvcResult result = mvc.perform(buildCreateAccountRequest(1, 0.0)).andReturn();

		assertEquals(NOT_FOUND.value(), result.getResponse().getStatus());
		verify(userService).retrieveUser(eq(1L));
		verifyNoMoreInteractions(userService);
	}

	private RequestBuilder buildCreateAccountRequest(long userId, double initialCredit) throws JsonProcessingException {
		return MockMvcRequestBuilders
				.post("/users/" + userId + "/accounts?initialCredit=" + initialCredit)
				.contentType(MediaType.APPLICATION_JSON);
	}

	private RequestBuilder buildGetInformationRequest(long userId) {
		return MockMvcRequestBuilders
				.get("/users/" + userId)
				.contentType(MediaType.APPLICATION_JSON);
	}
}
