How to build (requires maven, JDK 1.8): mvn clean package
How to run: java -jar target/spock-0.0.1-SNAPSHOT.jar

Check-out http://localhost:8080/swagger-ui.html for API information and also a way to try it out.

Some curl examples:
Create account for user: curl --include --request POST 'http://localhost:8080/users/1/accounts?initialCredit=10'
Get user information and transactions: curl --include --request GET 'http://localhost:8080/users/1'

Check-out http://localhost:8080/h2-console/ for access to the in-memory db. Login info in application.properties

Notes:
Unfortunately I had far less time to work on this than I had hoped for so the result is pretty basic. Hopefully it will
be enough for the start of a conversation. :) There are many things I would have still wanted to do like more thorough 
testing, proper git usage, more methods in the API (create users, transfer, etc.) but that will be for a future version. :)